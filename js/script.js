var supportsTouch = false;
$(document).ready(function () {

    setTimeout(autoscroll, 8000);

    //   main menu logic

    $('.mainMenuToggle__button-hamburger').click(function () {
        $('.mainMenuToggle').css('background-color', 'black');
        $('.mainMenuToggle__button-hamburger').html('<i class="fas fa-times"></i>');
        $('.mainMenu__list').toggleClass('mainMenu__list-visible').slideToggle(500, function () {
            if ($(this).css('display') === 'none') {
                $('.mainMenuToggle__button-hamburger').html('<i class="fas fa-bars"></i>');
                $(this).removeAttr('style');
                $('.mainMenuToggle').css('background-color', 'transparent');

            }
        });

    });

    //   play button logic

    $('.hero__button-play').click(function () {
        if ($('.hero__button-play').is('.playbackIsOn')) {
            $('.hero__button-play').html('<i class="fas fa-play"></i>').toggleClass('playbackIsOn');
        } else {
            $('.hero__button-play').html('<i class="fas fa-pause"></i>').toggleClass('playbackIsOn');
        }
    });

    //   clear menus while resizing

    var $window = $(window),
        $mMenu = $('.mainMenu__list');
    $window.resize(function resize() {
        if ($window.width() > 540) {
            $('.mainMenuToggle').css('background-color', 'transparent');
            $mMenu.removeClass('mainMenu__list-visible').removeAttr('style');
            $('.mainMenuToggle__button-hamburger').html('<i class="fas fa-bars"></i>');
        }
    }).trigger('resize');

    $('.hero__button-howItWorks').click(function () {
        $([document.documentElement, document.body]).animate({
            scrollTop: $('.howItWorks__heading').offset().top
        }, 500);
    });

    //   slider's buttons

    $('.testimonialSlider__button').click(function () {
        var x = $(this).index();
        $('.testimonialSlider__button-active').removeClass('testimonialSlider__button-active');
        $('.testimonialSlide__active').removeClass('testimonialSlide__active');
        $('.testimonialSlide').eq(x).addClass('testimonialSlide__active');
        $('.testimonialSlider__button').eq(x).addClass('testimonialSlider__button-active');
    });

    //   autoscroll slider

    var $numberOfSlides = $('.testimonialSlide').length;
    var $slideMaxIndex = $numberOfSlides - 1;

    function autoscroll() {
        var $currentActiveSlide = $('.testimonialSlide__active').index();
        if ($currentActiveSlide < $slideMaxIndex) {
            $('.testimonialSlide__active').removeClass('testimonialSlide__active');
            $('.testimonialSlide').eq($currentActiveSlide + 1).addClass('testimonialSlide__active');
            $('.testimonialSlider__button-active').removeClass('testimonialSlider__button-active');
            $('.testimonialSlider__button').eq($currentActiveSlide + 1).addClass('testimonialSlider__button-active');
        } else {
            $('.testimonialSlide__active').removeClass('testimonialSlide__active');
            $('.testimonialSlide').eq(0).addClass('testimonialSlide__active');
            $('.testimonialSlider__button-active').removeClass('testimonialSlider__button-active');
            $('.testimonialSlider__button').eq(0).addClass('testimonialSlider__button-active');
        }
        console.log('scrolled');
        setTimeout(autoscroll, 8000);
    }

    //   ios clickfix

    if ('ontouchstart' in window)
        supportsTouch = true;
    if (supportsTouch) {
        $('body').addClass('smartphone')
    }
});

